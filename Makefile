.PHONY: watch
watch:	
	cd backend; npm run watch

.PHONY: devsrv
devsrv:
	cd backend; npm run dev

.PHONY: devweb
devweb:
	cd frontend; npm run dev