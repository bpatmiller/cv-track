import gql from "graphql-tag";
import * as Urql from "urql";
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K];
};
export type MakeOptional<T, K extends keyof T> = Omit<
  T,
  K
> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]: Maybe<T[SubKey]>;
};
export type Omit<T, K extends keyof T> = Pick<
  T,
  Exclude<keyof T, K>
>;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type Entry = {
  __typename?: "Entry";
  createdAt: Scalars["String"];
  creatorId: Scalars["Float"];
  id: Scalars["Int"];
  tag: Scalars["String"];
  text: Scalars["String"];
  updatedAt: Scalars["String"];
};

export type FieldError = {
  __typename?: "FieldError";
  field: Scalars["String"];
  message: Scalars["String"];
};

export type Mutation = {
  __typename?: "Mutation";
  changePassword: UserResponse;
  createEntry: Entry;
  createPost: Post;
  deleteEntry: Scalars["Boolean"];
  deletePost: Scalars["Boolean"];
  forgotPassword: Scalars["Boolean"];
  login: UserResponse;
  logout: Scalars["Boolean"];
  register: UserResponse;
  updatePost?: Maybe<Post>;
};

export type MutationChangePasswordArgs = {
  newPassword: Scalars["String"];
  token: Scalars["String"];
};

export type MutationCreateEntryArgs = {
  text: Scalars["String"];
};

export type MutationCreatePostArgs = {
  title: Scalars["String"];
};

export type MutationDeleteEntryArgs = {
  id: Scalars["Float"];
};

export type MutationDeletePostArgs = {
  id: Scalars["Float"];
};

export type MutationForgotPasswordArgs = {
  email: Scalars["String"];
};

export type MutationLoginArgs = {
  password: Scalars["String"];
  usernameOrEmail: Scalars["String"];
};

export type MutationRegisterArgs = {
  options: UserCredentials;
};

export type MutationUpdatePostArgs = {
  id: Scalars["Float"];
  title: Scalars["String"];
};

export type PaginatedEntries = {
  __typename?: "PaginatedEntries";
  entries: Array<Entry>;
  hasMore: Scalars["Boolean"];
};

export type Post = {
  __typename?: "Post";
  createdAt: Scalars["String"];
  creatorId: Scalars["Float"];
  id: Scalars["Int"];
  text: Scalars["String"];
  title: Scalars["String"];
  updatedAt: Scalars["String"];
};

export type Query = {
  __typename?: "Query";
  entries: PaginatedEntries;
  me?: Maybe<User>;
  post?: Maybe<Post>;
  posts: Array<Post>;
};

export type QueryEntriesArgs = {
  cursor?: Maybe<Scalars["String"]>;
  limit: Scalars["Int"];
};

export type QueryPostArgs = {
  id: Scalars["Int"];
};

export type User = {
  __typename?: "User";
  createdAt: Scalars["String"];
  email: Scalars["String"];
  id: Scalars["Int"];
  updatedAt: Scalars["String"];
  username: Scalars["String"];
};

export type UserCredentials = {
  email: Scalars["String"];
  password: Scalars["String"];
  username: Scalars["String"];
};

export type UserResponse = {
  __typename?: "UserResponse";
  errors?: Maybe<Array<FieldError>>;
  user?: Maybe<User>;
};

export type RegularErrorFragment = {
  __typename?: "FieldError";
} & Pick<FieldError, "field" | "message">;

export type RegularUserResponseFragment = {
  __typename?: "UserResponse";
} & {
  errors?: Maybe<
    Array<
      { __typename?: "FieldError" } & RegularErrorFragment
    >
  >;
  user?: Maybe<
    { __typename?: "User" } & RegularUserFragment
  >;
};

export type RegularUserFragment = {
  __typename?: "User";
} & Pick<User, "id" | "username">;

export type ChangePasswordMutationVariables = Exact<{
  token: Scalars["String"];
  newPassword: Scalars["String"];
}>;

export type ChangePasswordMutation = {
  __typename?: "Mutation";
} & {
  changePassword: {
    __typename?: "UserResponse";
  } & RegularUserResponseFragment;
};

export type CreateEntryMutationVariables = Exact<{
  text: Scalars["String"];
}>;

export type CreateEntryMutation = {
  __typename?: "Mutation";
} & {
  createEntry: { __typename?: "Entry" } & Pick<
    Entry,
    "id" | "tag" | "text" | "creatorId"
  >;
};

export type DeleteEntryMutationVariables = Exact<{
  id: Scalars["Float"];
}>;

export type DeleteEntryMutation = {
  __typename?: "Mutation";
} & Pick<Mutation, "deleteEntry">;

export type ForgotPasswordMutationVariables = Exact<{
  email: Scalars["String"];
}>;

export type ForgotPasswordMutation = {
  __typename?: "Mutation";
} & Pick<Mutation, "forgotPassword">;

export type LoginMutationVariables = Exact<{
  usernameOrEmail: Scalars["String"];
  password: Scalars["String"];
}>;

export type LoginMutation = { __typename?: "Mutation" } & {
  login: {
    __typename?: "UserResponse";
  } & RegularUserResponseFragment;
};

export type LogoutMutationVariables = Exact<{
  [key: string]: never;
}>;

export type LogoutMutation = {
  __typename?: "Mutation";
} & Pick<Mutation, "logout">;

export type RegisterMutationVariables = Exact<{
  options: UserCredentials;
}>;

export type RegisterMutation = {
  __typename?: "Mutation";
} & {
  register: {
    __typename?: "UserResponse";
  } & RegularUserResponseFragment;
};

export type EntriesQueryVariables = Exact<{
  limit: Scalars["Int"];
  cursor?: Maybe<Scalars["String"]>;
}>;

export type EntriesQuery = { __typename?: "Query" } & {
  entries: { __typename?: "PaginatedEntries" } & Pick<
    PaginatedEntries,
    "hasMore"
  > & {
      entries: Array<
        { __typename?: "Entry" } & Pick<
          Entry,
          | "id"
          | "text"
          | "tag"
          | "createdAt"
          | "updatedAt"
          | "creatorId"
        >
      >;
    };
};

export type MeQueryVariables = Exact<{
  [key: string]: never;
}>;

export type MeQuery = { __typename?: "Query" } & {
  me?: Maybe<
    { __typename?: "User" } & Pick<User, "id" | "username">
  >;
};

export type PostsQueryVariables = Exact<{
  [key: string]: never;
}>;

export type PostsQuery = { __typename?: "Query" } & {
  posts: Array<
    { __typename?: "Post" } & Pick<
      Post,
      "id" | "createdAt" | "updatedAt" | "title"
    >
  >;
};

export const RegularErrorFragmentDoc = gql`
  fragment RegularError on FieldError {
    field
    message
  }
`;
export const RegularUserFragmentDoc = gql`
  fragment RegularUser on User {
    id
    username
  }
`;
export const RegularUserResponseFragmentDoc = gql`
  fragment RegularUserResponse on UserResponse {
    errors {
      ...RegularError
    }
    user {
      ...RegularUser
    }
  }
  ${RegularErrorFragmentDoc}
  ${RegularUserFragmentDoc}
`;
export const ChangePasswordDocument = gql`
  mutation ChangePassword(
    $token: String!
    $newPassword: String!
  ) {
    changePassword(
      token: $token
      newPassword: $newPassword
    ) {
      ...RegularUserResponse
    }
  }
  ${RegularUserResponseFragmentDoc}
`;

export function useChangePasswordMutation() {
  return Urql.useMutation<
    ChangePasswordMutation,
    ChangePasswordMutationVariables
  >(ChangePasswordDocument);
}
export const CreateEntryDocument = gql`
  mutation CreateEntry($text: String!) {
    createEntry(text: $text) {
      id
      tag
      text
      creatorId
    }
  }
`;

export function useCreateEntryMutation() {
  return Urql.useMutation<
    CreateEntryMutation,
    CreateEntryMutationVariables
  >(CreateEntryDocument);
}
export const DeleteEntryDocument = gql`
  mutation DeleteEntry($id: Float!) {
    deleteEntry(id: $id)
  }
`;

export function useDeleteEntryMutation() {
  return Urql.useMutation<
    DeleteEntryMutation,
    DeleteEntryMutationVariables
  >(DeleteEntryDocument);
}
export const ForgotPasswordDocument = gql`
  mutation ForgotPassword($email: String!) {
    forgotPassword(email: $email)
  }
`;

export function useForgotPasswordMutation() {
  return Urql.useMutation<
    ForgotPasswordMutation,
    ForgotPasswordMutationVariables
  >(ForgotPasswordDocument);
}
export const LoginDocument = gql`
  mutation Login(
    $usernameOrEmail: String!
    $password: String!
  ) {
    login(
      usernameOrEmail: $usernameOrEmail
      password: $password
    ) {
      ...RegularUserResponse
    }
  }
  ${RegularUserResponseFragmentDoc}
`;

export function useLoginMutation() {
  return Urql.useMutation<
    LoginMutation,
    LoginMutationVariables
  >(LoginDocument);
}
export const LogoutDocument = gql`
  mutation Logout {
    logout
  }
`;

export function useLogoutMutation() {
  return Urql.useMutation<
    LogoutMutation,
    LogoutMutationVariables
  >(LogoutDocument);
}
export const RegisterDocument = gql`
  mutation Register($options: UserCredentials!) {
    register(options: $options) {
      ...RegularUserResponse
    }
  }
  ${RegularUserResponseFragmentDoc}
`;

export function useRegisterMutation() {
  return Urql.useMutation<
    RegisterMutation,
    RegisterMutationVariables
  >(RegisterDocument);
}
export const EntriesDocument = gql`
  query Entries($limit: Int!, $cursor: String) {
    entries(limit: $limit, cursor: $cursor) {
      hasMore
      entries {
        id
        text
        tag
        createdAt
        updatedAt
        creatorId
      }
    }
  }
`;

export function useEntriesQuery(
  options: Omit<
    Urql.UseQueryArgs<EntriesQueryVariables>,
    "query"
  > = {}
) {
  return Urql.useQuery<EntriesQuery>({
    query: EntriesDocument,
    ...options,
  });
}
export const MeDocument = gql`
  query Me {
    me {
      id
      username
    }
  }
`;

export function useMeQuery(
  options: Omit<
    Urql.UseQueryArgs<MeQueryVariables>,
    "query"
  > = {}
) {
  return Urql.useQuery<MeQuery>({
    query: MeDocument,
    ...options,
  });
}
export const PostsDocument = gql`
  query Posts {
    posts {
      id
      createdAt
      updatedAt
      title
    }
  }
`;

export function usePostsQuery(
  options: Omit<
    Urql.UseQueryArgs<PostsQueryVariables>,
    "query"
  > = {}
) {
  return Urql.useQuery<PostsQuery>({
    query: PostsDocument,
    ...options,
  });
}
