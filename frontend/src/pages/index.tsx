import {
  Box,
  Button,
  Stack,
  StackDivider,
  Text,
} from "@chakra-ui/react";
import { Form, Formik } from "formik";
import { withUrqlClient } from "next-urql";
import { useRouter } from "next/router";
import React from "react";
import { EntryStack } from "../components/EntryCard";
import { InputField } from "../components/InputField";
import { Layout } from "../components/Layout";
import {
  useCreateEntryMutation,
  useEntriesQuery,
  useDeleteEntryMutation,
} from "../generated/graphql";
import { createUrqlClient } from "../utils/createUrqlClient";
import { useIsAuth } from "../utils/useIsAuth";

const Index = () => {
  const router = useRouter();
  useIsAuth();
  const [, createEntry] = useCreateEntryMutation();
  const [{ data }] = useEntriesQuery({
    variables: {
      limit: 10,
    },
  });
  return (
    <Layout variant="large">
      <Formik
        initialValues={{ text: "" }}
        onSubmit={async (values, { resetForm }) => {
          const response = await createEntry(values);
          if (response.error) {
          }
          resetForm();
        }}
      >
        <Form>
          <Box mt={4}>
            <InputField
              name="text"
              label="add new log"
              type="text"
              placeholder="placeholder"
              inputVariant="textarea"
            />
          </Box>
          <Button type="submit">submit</Button>
        </Form>
      </Formik>

      {/* display entries */}
      <Stack
        divider={<StackDivider borderColor="gray.200" />}
      >
        {EntryStack(data)}
      </Stack>
    </Layout>
  );
};

export default withUrqlClient(createUrqlClient, {
  ssr: false,
})(Index);
