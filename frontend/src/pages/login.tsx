import { Box } from "@chakra-ui/react";
import { Button, Flex, Spacer } from "@chakra-ui/react";
import { Form, Formik } from "formik";
import { withUrqlClient } from "next-urql";
import NextLink from "next/link";
import { useRouter } from "next/router";
import React from "react";
import { InputField } from "../components/InputField";
import { Layout } from "../components/Layout";
import { useLoginMutation } from "../generated/graphql";
import { createUrqlClient } from "../utils/createUrqlClient";
import { toErrorMap } from "../utils/toErrorMap";
const Login: React.FC<{}> = ({}) => {
  const router = useRouter();
  const [, login] = useLoginMutation();
  return (
    <Layout variant="small">
      <Formik
        initialValues={{
          usernameOrEmail: "",
          password: "",
        }}
        onSubmit={async (values, { setErrors }) => {
          const response = await login(values);
          if (response.data?.login.errors) {
            setErrors(
              toErrorMap(response.data.login.errors)
            );
          } else if (response.data?.login.user) {
            router.push("/");
          }
        }}
      >
        {({ isSubmitting }) => (
          <Form>
            <Box mt={4}>
              <InputField
                name="usernameOrEmail"
                label="username or email"
                placeholder="steelersfan1@live.com"
              />
            </Box>
            <Box mt={4}>
              <InputField
                name="password"
                label="password"
                type="password"
                placeholder="*********"
              />
            </Box>
            <Flex mt={4}>
              <Button
                isLoading={isSubmitting}
                type="submit"
              >
                login
              </Button>
              <Spacer />
              <NextLink href="/forgot-password">
                <Button variant="ghost">i forgot</Button>
              </NextLink>
            </Flex>
          </Form>
        )}
      </Formik>
    </Layout>
  );
};

export default withUrqlClient(createUrqlClient)(Login);
