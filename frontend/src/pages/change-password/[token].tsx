import {
  Alert,
  AlertIcon,
  Box,
  Button,
  Link,
} from "@chakra-ui/react";
import { Form, Formik } from "formik";
import { NextPage } from "next";
import NextLink from "next";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { InputField } from "../../components/InputField";
import { Wrapper } from "../../components/Wrapper";
import { useChangePasswordMutation } from "../../generated/graphql";
import { createUrqlClient } from "../../utils/createUrqlClient";
import { toErrorMap } from "../../utils/toErrorMap";
import { withUrqlClient } from "next-urql";

export const ChangePassword: NextPage<{
  token: string;
}> = ({ token }) => {
  const router = useRouter();
  const [, changePassword] = useChangePasswordMutation();
  const [tokenError, setTokenError] = useState("");
  return (
    <Wrapper variant="small">
      <Formik
        initialValues={{
          newPassword: "",
        }}
        onSubmit={async (values, { setErrors }) => {
          const response = await changePassword({
            newPassword: values.newPassword,
            token,
          });
          if (response.data?.changePassword.errors) {
            const errorMap = toErrorMap(
              response.data.changePassword.errors
            );
            if ("token" in errorMap) {
              setTokenError(errorMap.token);
            }
            setErrors(errorMap);
          } else if (response.data?.changePassword.user) {
            router.push("/");
          }
        }}
      >
        {({ isSubmitting }) => (
          <Form>
            {tokenError && (
              <Box>
                <Alert status="error">
                  <AlertIcon />
                  token error
                </Alert>
                {/* <NextLink href="/forgot-password"> */}
                <Link>reset password again</Link>
                {/* </NextLink> */}
              </Box>
            )}
            <Box mt={4}>
              <InputField
                name="newPassword"
                label="New password"
                type="newPassword"
                placeholder="*********"
              />
            </Box>
            <Box mt={4}>
              <Button
                isLoading={isSubmitting}
                type="submit"
                colorScheme="messenger"
              >
                Submit
              </Button>
            </Box>
          </Form>
        )}
      </Formik>
    </Wrapper>
  );
};

ChangePassword.getInitialProps = ({ query }) => {
  return {
    token: query.token as string,
  };
};

export default withUrqlClient(
  createUrqlClient,
  {}
)(ChangePassword);
