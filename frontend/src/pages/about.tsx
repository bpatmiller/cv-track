import {
  Box,
  Heading,
  Stack,
  Text,
} from "@chakra-ui/react";
import { withUrqlClient } from "next-urql";
import React from "react";
import { Layout } from "../components/Layout";
import { createUrqlClient } from "../utils/createUrqlClient";

function Feature({
  title,
  desc,
  ...rest
}: {
  title: string;
  desc: any;
}) {
  return (
    <Box p={5} shadow="md" borderWidth="1px" {...rest}>
      <Heading fontSize="xl">{title}</Heading>
      {desc}
    </Box>
  );
}

const Index = () => {
  return (
    <Layout variant="small">
      <Stack>
        <Feature
          title="About"
          desc={
            <Text>CV Track is a note taking web app</Text>
          }
        ></Feature>

        <Feature
          title="Tech Stack"
          desc={
            <Text>
              Running Apollo Server (GraphQL) on Node with
              Redis on backend. Running Postgres for
              database. Running React with Chakra UI library
              and URQL GraphQL client for frontend. Frontend
              hosted on Vercel, backend hosted on
              DigitalOcean with Dokku.
            </Text>
          }
        ></Feature>
      </Stack>
    </Layout>
  );
};

export default withUrqlClient(createUrqlClient, {
  ssr: true,
})(Index);
