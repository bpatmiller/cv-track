import { Button, Box } from "@chakra-ui/react";
import { Form, Formik } from "formik";
import { withUrqlClient } from "next-urql";
import { useRouter } from "next/router";
import React from "react";
import { InputField } from "../components/InputField";
import { Layout } from "../components/Layout";
import { useRegisterMutation } from "../generated/graphql";
import { createUrqlClient } from "../utils/createUrqlClient";
import { toErrorMap } from "../utils/toErrorMap";

interface registerProps {}

const Register: React.FC<registerProps> = ({}) => {
  const router = useRouter();
  const [, register] = useRegisterMutation();
  return (
    <Layout variant="small">
      <Formik
        initialValues={{
          email: "",
          username: "",
          password: "",
        }}
        onSubmit={async (values, { setErrors }) => {
          const response = await register({
            options: values,
          });
          if (response.data?.register.errors) {
            setErrors(
              toErrorMap(response.data.register.errors)
            );
          } else if (response.data?.register.user) {
            router.push("/");
          }
        }}
      >
        {({ isSubmitting }) => (
          <Form>
            <Box mt={4}>
              <InputField
                name="email"
                label="email"
                placeholder="jim@space.net"
              />
            </Box>
            <Box mt={4}>
              <InputField
                name="username"
                label="username"
                placeholder="jim1932"
              />
            </Box>
            <Box mt={4}>
              <InputField
                name="password"
                label="password"
                type="password"
                placeholder="********"
              />
            </Box>
            <Box mt={4}>
              <Button
                isLoading={isSubmitting}
                type="submit"
              >
                register
              </Button>
            </Box>
          </Form>
        )}
      </Formik>
    </Layout>
  );
};

export default withUrqlClient(createUrqlClient)(Register);
