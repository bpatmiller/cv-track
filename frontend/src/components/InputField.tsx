import {
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
  Textarea,
} from "@chakra-ui/react";
import { useField } from "formik";
import React, { InputHTMLAttributes } from "react";

type InputFieldProps =
  InputHTMLAttributes<HTMLInputElement> & {
    name: string;
    label: string;
    inputVariant?: "input" | "textarea";
  };

export const InputField: React.FC<InputFieldProps> = ({
  label,
  size: _,
  ...props
}) => {
  const [field, { error }] = useField(props);
  return (
    <FormControl isInvalid={!!error}>
      <FormLabel htmlFor={field.name}>{label}</FormLabel>
      {props.inputVariant === "textarea" ? (
        <Textarea {...field} id={field.name} />
      ) : (
        <Input {...field} {...props} id={field.name} />
      )}

      {error ? (
        <FormErrorMessage>{error}</FormErrorMessage>
      ) : null}
    </FormControl>
  );
};
