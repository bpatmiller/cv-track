import {
  Box,
  Button,
  Flex,
  Spacer,
  Text,
} from "@chakra-ui/react";
import NextLink from "next/link";
import React from "react";
import { ColormodeToggle } from "../components/ColormodeToggle";
import {
  useLogoutMutation,
  useMeQuery,
} from "../generated/graphql";

interface NavBarProps {}

export const NavBar: React.FC<NavBarProps> = ({}) => {
  const [, logout] = useLogoutMutation();
  const [{ data, fetching }] = useMeQuery();
  let body = null;
  if (fetching) {
  } else if (!data?.me) {
    body = (
      <>
        <NextLink href="/about">
          <Button mr="2">about</Button>
        </NextLink>
        <NextLink href="/login">
          <Button mr="2">login</Button>
        </NextLink>
        <NextLink href="/register">
          <Button mr="2">register</Button>
        </NextLink>
      </>
    );
  } else {
    body = (
      <>
        {/* <Flex direction="column" align="flex-start">
          <Box> */}
        <Text
          m={0}
          p={2}
          textAlign={"center"}
          fontWeight={600}
        >
          hello {data?.me?.username}
        </Text>
        <NextLink href="/analytics">
          <Button mr="2">analytics</Button>
        </NextLink>
        <NextLink href="/about">
          <Button mr="2">about</Button>
        </NextLink>
        <Button
          onClick={() => {
            logout();
          }}
        >
          logout
        </Button>
      </>
    );
  }

  return (
    <Flex
      direction="column"
      alignItems="center"
      justifyContent="center"
      w={["100%", "100%", 224]}
      flexShrink={0}
      flexGrow={0}
    >
      <Box pt={[2, 2, 4]}>
        <Text
          textAlign={["center", "center", "left"]}
          as="i"
          fontSize={"4xl"}
          fontWeight="800"
          m={0}
          p={4}
        >
          CV Track
        </Text>
      </Box>
      <Flex
        w="100%"
        justifyContent={"center"}
        direction={["row", "row", "column"]}
      >
        {body}
        <ColormodeToggle />
      </Flex>
    </Flex>
  );
};
