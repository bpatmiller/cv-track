import { MoonIcon, SunIcon } from "@chakra-ui/icons";
import {
  Button,
  useColorMode,
  useColorModeValue,
} from "@chakra-ui/react";
import React from "react";

interface ColormodeToggleProps {}

export const ColormodeToggle: React.FC<
  ColormodeToggleProps
> = ({}) => {
  const { toggleColorMode } = useColorMode();
  const Icon = useColorModeValue(MoonIcon, SunIcon);
  return (
    <Button onClick={toggleColorMode}>{<Icon />}</Button>
  );
};
