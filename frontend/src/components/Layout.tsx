import { Flex } from "@chakra-ui/react";
import React from "react";
import { NavBar } from "./NavBar";

interface LayoutProps {
  variant: "small" | "large";
}

export const Layout: React.FC<LayoutProps> = ({
  children,
  variant,
}) => {
  return (
    <Flex
      direction={["column", "column", "row"]}
      alignItems={"flex-start"}
    >
      <NavBar />
      <Flex
        direction={"column"}
        padding={[2, 2, 8]}
        w="100%"
      >
        {children}
      </Flex>
    </Flex>
  );
};
