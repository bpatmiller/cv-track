import {
  Box,
  Text,
  Button,
  Flex,
  useColorModeValue,
} from "@chakra-ui/react";
import {
  useDeleteEntryMutation,
  Entry,
  EntriesQuery,
} from "../generated/graphql";

export const EntryStack = (
  data: EntriesQuery | undefined
) => {
  const [, deleteEntry] = useDeleteEntryMutation();

  return !data ? (
    <div>loading...</div>
  ) : (
    <Box>
      {data!.entries.entries.map((e) =>
        !e ? null : EntryCard(e, deleteEntry)
      )}
    </Box>
  );
};

export const EntryCard = (e: Entry, deleteEntry: any) => {
  const accentColor = useColorModeValue(
    "green.50",
    "green.900"
  );
  return (
    <Box shadow="md" borderWidth="0px" key={e.id}>
      <Text
        textAlign="center"
        w="100%"
        bgColor={accentColor}
      >
        {new Date(parseInt(e.createdAt))
          .toISOString()
          .slice(0, 19)
          .replace(/-/g, "/")
          .replace("T", " - ")}
      </Text>
      <Text>{e.text}</Text>
      {/* <Text>tag:{e.tag}</Text> */}
      <Flex justifyContent={"center"} w="100%">
        <Button
          onClick={() => {
            if (e.id) {
              deleteEntry({
                id: e.id,
              });
            }
          }}
        >
          delete
        </Button>
        {/* <Button>edit</Button> */}
      </Flex>
    </Box>
  );
};
