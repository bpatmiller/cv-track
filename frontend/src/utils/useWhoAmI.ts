import { useMeQuery } from "../generated/graphql";
import { useEffect } from "react";

export const useWhoAmI = () => {
  const [{ data, fetching }] = useMeQuery();
  return data?.me;
};
