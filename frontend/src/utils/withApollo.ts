import { createWithApollo } from "./createWithApollo";
import {
  ApolloClient,
  InMemoryCache,
} from "@apollo/client";
import { NextPageContext } from "next";
import { PaginatedEntries } from "../generated/graphql";

const createClient = (ctx: NextPageContext) =>
  new ApolloClient({
    uri: process.env.NEXT_PUBLIC_API_URL as string,
    credentials: "include",
    headers: {
      cookie:
        (typeof window === "undefined"
          ? ctx?.req?.headers.cookie
          : undefined) || "",
    },
    cache: new InMemoryCache({
      typePolicies: {
        Query: {
          fields: {
            entries: {
              keyArgs: [],
              merge(
                existing: PaginatedEntries | undefined,
                incoming: PaginatedEntries
              ): PaginatedEntries {
                return {
                  ...incoming,
                  entries: [
                    ...(existing?.entries || []),
                    ...incoming.entries,
                  ],
                };
              },
            },
          },
        },
      },
    }),
  });

export const withApollo = createWithApollo(createClient);
