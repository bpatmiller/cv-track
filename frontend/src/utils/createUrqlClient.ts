// import { dedupExchange, fetchExchange } from "urql";
import {
  dedupExchange,
  fetchExchange,
  stringifyVariables,
} from "urql";
import {
  cacheExchange,
  Resolver,
} from "@urql/exchange-graphcache";
import {
  LoginMutation,
  LogoutMutation,
  MeDocument,
  MeQuery,
  RegisterMutation,
} from "../generated/graphql";
import { betterUpdateQuery } from "./betterUpdateQuery";
import { isServer } from "./isServer";

const cursorPagination = (): Resolver => {
  return (_parent, fieldArgs, cache, info) => {
    const { parentKey: entityKey, fieldName } = info;
    const allFields = cache.inspectFields(entityKey);
    const fieldInfos = allFields.filter(
      (info) => info.fieldName === fieldName
    );
    const size = fieldInfos.length;
    if (size === 0) {
      return undefined;
    }

    const fieldKey = `${fieldName}(${stringifyVariables(
      fieldArgs
    )})`;
    const isItInTheCache = cache.resolve(
      cache.resolve(entityKey, fieldKey) as string,
      "entries"
    );
    info.partial = !isItInTheCache;
    let hasMore = true;
    const results: string[] = [];
    fieldInfos.forEach((fi) => {
      const key = cache.resolveFieldByKey(
        entityKey,
        fi.fieldKey
      ) as string;
      const data = cache.resolve(
        key,
        "entries"
      ) as string[];
      const _hasMore = cache.resolve(key, "hasMore");
      if (!_hasMore) {
        hasMore = _hasMore as boolean;
      }
      results.push(...data);
    });

    return {
      __typename: "PaginatedEntries",
      hasMore,
      entries: results,
    };
  };
};

export const createUrqlClient = (
  ssrExchange: any,
  ctx: any
) => {
  let cookie = "";
  if (isServer()) {
    cookie = ctx?.req?.headers?.cookie;
  }
  return {
    url: process.env.NEXT_PUBLIC_API_URL as string,
    fetchOptions: {
      credentials: "include" as const,
      headers: cookie
        ? {
            cookie,
          }
        : undefined,
    },
    exchanges: [
      dedupExchange,
      cacheExchange({
        keys: {
          PaginatedEntries: () => null,
        },
        resolvers: {
          Query: {
            entries: cursorPagination(),
          },
        },
        updates: {
          Mutation: {
            logout: (_result, _args, cache, _info) => {
              betterUpdateQuery<LogoutMutation, MeQuery>(
                cache,
                { query: MeDocument },
                _result,
                () => {
                  const allFields =
                    cache.inspectFields("Query");
                  const fieldInfos = allFields.filter(
                    (info) => info.fieldName === "entries"
                  );
                  fieldInfos.forEach((fi) => {
                    cache.invalidate(
                      "Query",
                      "entries",
                      fi.arguments || {}
                    );
                  });
                  return {
                    me: null,
                  };
                }
              );
            },
            login: (_result, _args, cache) => {
              betterUpdateQuery<LoginMutation, MeQuery>(
                cache,
                { query: MeDocument },
                _result,
                (result, query) => {
                  if (result.login.errors) {
                    return query;
                  } else {
                    return {
                      me: result.login.user,
                    };
                  }
                }
              );
            },
            register: (_result, _args, cache) => {
              betterUpdateQuery<RegisterMutation, MeQuery>(
                cache,
                { query: MeDocument },
                _result,
                (result, query) => {
                  if (result.register.errors) {
                    return query;
                  } else {
                    return {
                      me: result.register.user,
                    };
                  }
                }
              );
            },
            createEntry: (_result, args, cache, info) => {
              const allFields =
                cache.inspectFields("Query");
              const fieldInfos = allFields.filter(
                (info) => info.fieldName === "entries"
              );
              fieldInfos.forEach((fi) => {
                cache.invalidate(
                  "Query",
                  "entries",
                  fi.arguments || {}
                );
              });
            },
            deleteEntry: (_result, args, cache, info) => {
              const allFields =
                cache.inspectFields("Query");
              const fieldInfos = allFields.filter(
                (info) => info.fieldName === "entries"
              );
              fieldInfos.forEach((fi) => {
                cache.invalidate(
                  "Query",
                  "entries",
                  fi.arguments || {}
                );
              });
            },
          },
        },
      }),
      ssrExchange,
      fetchExchange,
    ],
  };
};
