import { isAuth } from "../middleware/isAuth";
import {
  Arg,
  Ctx,
  Field,
  Int,
  Mutation,
  ObjectType,
  Query,
  Resolver,
  UseMiddleware,
} from "type-graphql";
import { Entry } from "../entities/Entry";
import { MyContext } from "src/types";
import { getConnection } from "typeorm";
import { EntryTag } from "../entities/Entry";

@ObjectType()
class PaginatedEntries {
  @Field(() => [Entry])
  entries: Entry[];
  @Field()
  hasMore: boolean;
}

@Resolver()
export class EntryResolver {
  @Query(() => PaginatedEntries)
  @UseMiddleware(isAuth)
  async entries(
    @Arg("limit", () => Int) limit: number,
    @Arg("cursor", () => String, { nullable: true })
    cursor: string,
    @Ctx() { req }: MyContext
  ): Promise<PaginatedEntries> {
    const safeLimit = Math.min(limit, 50);
    const qb = getConnection()
      .getRepository(Entry)
      .createQueryBuilder("e")
      .orderBy('"createdAt"', "DESC")
      .take(safeLimit);
    if (cursor) {
      qb.where('"createdAt" < :cursor', {
        cursor: new Date(parseInt(cursor)),
      });
    }
    qb.where('"creatorId" = :uid', {
      uid: req.session.UserID,
    });

    const entries = await qb.getMany();
    return { entries: entries, hasMore: true };
  }

  @Mutation(() => Entry)
  @UseMiddleware(isAuth)
  async createEntry(
    @Arg("text") text: string,
    @Ctx() { req }: MyContext
  ): Promise<Entry> {
    // find tag
    // let tags = [
    //   "music",
    //   "film",
    //   "reading",
    //   "journal",
    //   "devlog",
    // ];
    // for (let item in tags) {
    //   if (isNaN(Number(item))) {
    //     console.log(item);
    //   }
    // }
    let tag = [EntryTag.DEVLOG];
    return Entry.create({
      text: text,
      creatorId: req.session.UserID,
      // tag: tag,
    }).save();
  }
  @Mutation(() => Boolean)
  async deleteEntry(
    @Arg("id") id: number
  ): Promise<boolean> {
    await Entry.delete(id);
    return true;
  }
}
