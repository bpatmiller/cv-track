#!/bin/bash

echo $1
VERSION=$1
docker build -t bpatmiller/cvtrack:$VERSION .
docker push bpatmiller/cvtrack:$VERSION
ssh root@164.90.146.149 "docker pull bpatmiller/cvtrack:$VERSION && docker tag bpatmiller/cvtrack:$VERSION dokku/api:$VERSION && dokku deploy api $VERSION"